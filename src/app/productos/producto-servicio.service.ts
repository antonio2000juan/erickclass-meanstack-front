import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Producto } from './producto.interface';
import { Respuesta } from './producto.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductoServicioService {

  private Baseurl: string = environment.url

  constructor(private http:HttpClient) { }

  obtenerProductos(){
    const url = `${this.Baseurl}producto/obtener`;
    return this.http.get<Producto[]>(url);
  }

  registrarProducto(productito:Producto){
    const url = `${this.Baseurl}producto/registrar`;
    return this.http.post<Respuesta>(url, productito);
  }

}
