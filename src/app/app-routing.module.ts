import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MuestraComponent } from './productos/muestra/muestra.component';
import { RegistroComponent } from './productos/registro/registro.component';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import("./auth/auth.module").then(m => m.AuthModule)
  },
  {
    path: 'home',
    loadChildren: () => import("./protegido/protegido.module").then(m => m.ProtegidoModule)
  },
  {
    path: 'proreg', component: RegistroComponent
  },
  {
    path: 'proob', component: MuestraComponent
  },
  {
    path: '**',
    redirectTo: 'auth'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
